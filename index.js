const express = require('express');
const app = express();
const alexa = require("alexa-app");
const alexaApp = new alexa.app("nlacademyalexa");
const accountSid = process.env.TWILIO_SID;
const authToken = process.env.TWILIO_AUTH;
const client = require('twilio')(accountSid, authToken);
const _ = require('lodash');
const BASE_URL = 'https://nlacademyalexa.herokuapp.com';
const BASE_NUMBER = '+498928977671';

app.use(express.static('static'));

const phonebook = [{
	name: 'max',
	number: '+4917656582201'
}];

function getNumberForName(name) {
	for (let i=0;i<=phonebook.length;i++) {
		let contact = phonebook[i];
		if (name.toLowerCase() === contact.name) {
			return contact.number;
		}
	}

	return null;
}

function call(alexaRequest, alexaResponse) {
	console.log("calling");
	let name = alexaRequest.getSession().get("name");
	console.log("name: " + name);
	let message = alexaRequest.getSession().get("message");
	console.log("message: " + message);
	alexaResponse.shouldEndSession(true);
	if (!name) {
		alexaResponse.say('Please give me a name!');
		return;
	}
	if (!message) {
		alexaResponse.say('I could not understand a proper message!');
		return;
	}
	let number = getNumberForName(name);
	if (!number) {
		alexaResponse.say('I have no number for that name!');
		return;
	}
	console.log("will be calling: " + number);
	alexaResponse.say("Calling the person and fulfilling your wish!");
	alexaRequest.getSession().clear("name");
	alexaRequest.getSession().clear("message");
	client.calls.create({
		 url: BASE_URL + "/twilml?message=" + encodeURIComponent(message),
		 to: number,
		 from: BASE_NUMBER
	 }, function(err, call) {
		 console.log(err);
		 console.log(call.sid);
	 });
}

alexaApp.intent("call",
	function(alexaRequest, alexaResponse) {
		alexaRequest.getSession().set("name", alexaRequest.slot("name"));
		alexaRequest.getSession().set("message", alexaRequest.slot("message"));
		call(alexaRequest, alexaResponse);
	}
);

alexaApp.intent("AMAZON.StopIntent",
	function(alexaRequest, aleaxResponse) {
		alexaRequest.getSession().clear("name");
		alexaRequest.getSession().clear("message");
		aleaxResponse.say("Call cancelled.");
	}
);

alexaApp.intent("AMAZON.CancelIntent",
	function(alexaRequest, aleaxResponse) {
		alexaRequest.getSession().clear("name");
		alexaRequest.getSession().clear("message");
		aleaxResponse.say("Call cancelled.");
	}
);

alexaApp.launch(function(request, response) {
	response.shouldEndSession(false);
	response.say("To call someone please specify his name and the message.");
});

alexaApp.intent("AMAZON.HelpIntent",
	function(request, response) {
		response.say("To call someone please specify his name and the message.");
	}
);

app.post('/twilml', function(req, res, next) {
	const VoiceResponse = require('twilio').twiml.VoiceResponse;
	const response = new VoiceResponse();
	response.pause({
		length: 1
	});
	response.say(req.query.message);
	response.play({ loop: 1 }, BASE_URL + '/sound.mp3');
	console.log(response.toString());
	res.type('xml');
	res.send(response.toString());
});
alexaApp.express({expressApp: app});

const server = app.listen(process.env.PORT || 5001, function () {
	let host = server.address().address;
	let port = server.address().port;

	console.log('Listening at http://%s:%s', host, port);
});
